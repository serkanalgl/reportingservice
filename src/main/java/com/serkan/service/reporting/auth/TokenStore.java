package com.serkan.service.reporting.auth;

import org.springframework.stereotype.Component;

@Component
public class TokenStore {

	private String token;

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	@Override
	public String toString() {
		return "TokenStore [token=" + token + "]";
	}

}
