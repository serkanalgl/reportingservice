package com.serkan.service.reporting.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.serkan.service.reporting.handler.ProviderHandler;
import com.serkan.service.reporting.model.ProviderAuthResponse;

@Component
public class TokenSupplier {

	private static final Logger logger = LoggerFactory.getLogger(TokenSupplier.class);

	private TokenStore tokenStore;
	private ProviderHandler providerHandler;

	@Autowired
	public TokenSupplier(TokenStore tokenStore, ProviderHandler providerHandler) {
		this.tokenStore = tokenStore;
		this.providerHandler = providerHandler;
	}

	public String loadAccessToken() {

		ProviderAuthResponse providerLogin = providerHandler.getAccessToken();
		tokenStore.setToken(providerLogin.getToken());

		logger.info("the token was retrieved and is changed with the old one. new token: {}", providerLogin.getToken());

		return providerLogin.getToken();

	}

}
