package com.serkan.service.reporting.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTask.class);

	private TokenSupplier tokenSupplier;

	public ScheduledTask(TokenSupplier tokenSupplier) {
		this.tokenSupplier = tokenSupplier;
	}

	@Scheduled(fixedRate = 550000, initialDelay=550000)
	public void getProviderAccessToken() {
		logger.info("running scheduled task for renew provider auth token");
		tokenSupplier.loadAccessToken();
	}
}