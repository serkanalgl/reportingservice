package com.serkan.service.reporting.handler.impl.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;

public class RestUtil {

	public static HttpEntity<Map<String, Object>> getTransactionEntity(String transactionId) {
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("transactionId", transactionId);

		HttpEntity<Map<String, Object>> entity = new HttpEntity<Map<String, Object>>(payload);
		return entity;
	}
}
