package com.serkan.service.reporting.handler.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.serkan.service.reporting.beans.ApplicationConfig;
import com.serkan.service.reporting.handler.ProviderHandler;
import com.serkan.service.reporting.handler.impl.util.RestUtil;
import com.serkan.service.reporting.model.CustomerInfoResponse;
import com.serkan.service.reporting.model.MerchantResponse;
import com.serkan.service.reporting.model.ProviderAuthRequest;
import com.serkan.service.reporting.model.ProviderAuthResponse;
import com.serkan.service.reporting.model.TransactionQueryRequest;
import com.serkan.service.reporting.model.TransactionQueryResponse;
import com.serkan.service.reporting.model.TransactionReportRequest;
import com.serkan.service.reporting.model.TransactionReportResponse;
import com.serkan.service.reporting.model.TransactionResponse;

@Service
public class ProviderHandlerImpl implements ProviderHandler {

	private static final Logger logger = LoggerFactory.getLogger(ProviderHandler.class);

	private RestTemplate restTemplate;
	private ApplicationConfig appConfig;

	private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	public ProviderHandlerImpl(RestTemplate restTemplate, ApplicationConfig appConfig,
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter) {
		this.restTemplate = restTemplate;
		this.appConfig = appConfig;
		this.mappingJackson2HttpMessageConverter = mappingJackson2HttpMessageConverter;
	}

	@Override
	public ProviderAuthResponse getAccessToken() {

		logger.info("trying to get access token from provider");

		ProviderAuthRequest providerLogin = new ProviderAuthRequest(appConfig.getProviderAuthenticationEmail(),
				appConfig.getProviderAuthenticationPassword());

		HttpEntity<ProviderAuthRequest> entity = new HttpEntity<ProviderAuthRequest>(providerLogin);

		return restTemplate.postForObject(appConfig.getProviderUrl() + "/v3/merchant/user/login", entity,
				ProviderAuthResponse.class);
	}

	@Override
	public TransactionReportResponse getTransactionReport(TransactionReportRequest transactionReportRequest) {

		logger.info("trying to get transaction report");

		HttpEntity<TransactionReportRequest> entity = new HttpEntity<TransactionReportRequest>(
				transactionReportRequest);

		return restTemplate.postForObject(appConfig.getProviderUrl() + "/v3/transactions/report", entity,
				TransactionReportResponse.class);

	}

	@Override
	public TransactionResponse getTransactionById(String transactionId) {

		logger.info("trying to get transaction by id: {}", transactionId);

		return restTemplate.postForObject(appConfig.getProviderUrl() + "/v3/transaction",
				RestUtil.getTransactionEntity(transactionId), TransactionResponse.class);
	}

	@Override
	public CustomerInfoResponse getCustomerInfoOfTransaction(String transactionId) {

		logger.info("trying to get customer information of transaction by transactionId: {}", transactionId);

		return restTemplate.postForObject(appConfig.getProviderUrl() + "/v3/client",
				RestUtil.getTransactionEntity(transactionId), CustomerInfoResponse.class);
	}

	@Override
	public MerchantResponse getMerchantOfTransaction(String transactionId) {

		logger.info("trying to get merchant of transaction by transactionId: {}", transactionId);

		return restTemplate.postForObject(appConfig.getProviderUrl() + "/v3/merchant",
				RestUtil.getTransactionEntity(transactionId), MerchantResponse.class);
	}

	@Override
	public TransactionQueryResponse getTransactions(TransactionQueryRequest transactionQueryRequest) {
		logger.info("trying to get transactions");

		ObjectMapper objectMapper = mappingJackson2HttpMessageConverter.getObjectMapper();

		Map<String, Object> payload = objectMapper.convertValue(transactionQueryRequest, Map.class);

		//TODO: use Optinal java8 for making kind of request 

		HttpEntity<Map<String, Object>> entity = new HttpEntity<Map<String, Object>>(payload);

		return restTemplate.postForObject(appConfig.getProviderUrl() + "/v3/transaction/list", entity,
				TransactionQueryResponse.class);

	}

}
