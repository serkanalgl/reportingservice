package com.serkan.service.reporting.handler;

import com.serkan.service.reporting.model.CustomerInfoResponse;
import com.serkan.service.reporting.model.MerchantResponse;
import com.serkan.service.reporting.model.ProviderAuthResponse;
import com.serkan.service.reporting.model.TransactionQueryRequest;
import com.serkan.service.reporting.model.TransactionQueryResponse;
import com.serkan.service.reporting.model.TransactionReportRequest;
import com.serkan.service.reporting.model.TransactionReportResponse;
import com.serkan.service.reporting.model.TransactionResponse;

public interface ProviderHandler {

	ProviderAuthResponse getAccessToken();

	TransactionReportResponse getTransactionReport(TransactionReportRequest transactionReportRequest);

	TransactionResponse getTransactionById(String transactionId);

	CustomerInfoResponse getCustomerInfoOfTransaction(String transactionId);

	MerchantResponse getMerchantOfTransaction(String transactionId);
	
	TransactionQueryResponse getTransactions(TransactionQueryRequest transactionQueryRequest);
}
