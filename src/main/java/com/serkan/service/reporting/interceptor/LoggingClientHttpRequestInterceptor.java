package com.serkan.service.reporting.interceptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class LoggingClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

	protected Logger logger = LoggerFactory.getLogger(LoggingClientHttpRequestInterceptor.class);

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		traceRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		traceResponse(response);

		return response;
	}

	private void traceRequest(HttpRequest request, byte[] body) throws IOException {

		logger.info("sending a request to provider url:{} method:{} headers:{} body:{}", request.getURI(),
				request.getMethod(), request.getHeaders(), new String(body, "UTF-8"));

	}

	private void traceResponse(ClientHttpResponse response) throws IOException {
		StringBuilder inputStringBuilder = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
		String line = bufferedReader.readLine();
		while (line != null) {
			inputStringBuilder.append(line);
			inputStringBuilder.append('\n');
			line = bufferedReader.readLine();
		}

		logger.info("the response recieved from provider status code:{} status text:{} headers:{} body:{}",
				response.getStatusCode(), response.getStatusText(), response.getHeaders(),
				inputStringBuilder.toString());
	}

}