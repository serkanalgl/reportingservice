package com.serkan.service.reporting.interceptor;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.serkan.service.reporting.auth.TokenStore;

public class AuthenticationClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationClientHttpRequestInterceptor.class);

	private TokenStore tokenStore;

	public AuthenticationClientHttpRequestInterceptor(TokenStore tokenStore) {
		this.tokenStore = tokenStore;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		if (tokenStore.getToken() != null) {
			HttpHeaders headers = request.getHeaders();
			headers.add("Authorization", tokenStore.getToken());
		}

		return execution.execute(request, body);

	}

}
