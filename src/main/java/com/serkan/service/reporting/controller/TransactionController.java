package com.serkan.service.reporting.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import com.serkan.service.reporting.controller.api.TransactionApi;
import com.serkan.service.reporting.handler.ProviderHandler;
import com.serkan.service.reporting.model.CustomerInfoResponse;
import com.serkan.service.reporting.model.MerchantResponse;
import com.serkan.service.reporting.model.TransactionQueryRequest;
import com.serkan.service.reporting.model.TransactionQueryResponse;
import com.serkan.service.reporting.model.TransactionReportRequest;
import com.serkan.service.reporting.model.TransactionReportResponse;
import com.serkan.service.reporting.model.TransactionResponse;

@Controller
public class TransactionController implements TransactionApi{

	private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
	
	private ProviderHandler providerHandler;
	
	@Autowired
	public TransactionController(ProviderHandler providerHandler) {
		this.providerHandler = providerHandler;
	}
	
	@Override
	public ResponseEntity<TransactionReportResponse> getTransactionReport(@RequestBody @Valid TransactionReportRequest transactionReportREQ) {
		return ResponseEntity.ok(providerHandler.getTransactionReport(transactionReportREQ));
	}
	
	@Override
	public ResponseEntity<TransactionResponse> getTransactionById(String transactionId) {
		return ResponseEntity.ok(providerHandler.getTransactionById(transactionId)); 
	}
	
	@Override
	public ResponseEntity<CustomerInfoResponse> getCustomerInfoOfTransaction(String transactionId) {
		return ResponseEntity.ok(providerHandler.getCustomerInfoOfTransaction(transactionId));
	}
	
	@Override
	public ResponseEntity<MerchantResponse> getMerchantOfTransaction(String transactionId) {
		return ResponseEntity.ok(providerHandler.getMerchantOfTransaction(transactionId));
	}
	
	@Override
	public ResponseEntity<TransactionQueryResponse> getTransactions(@RequestBody @Valid TransactionQueryRequest transactionQueryRequest) {
		return ResponseEntity.ok(providerHandler.getTransactions(transactionQueryRequest));
	}
}
