package com.serkan.service.reporting.controller.beans;

public class ResponseBean {

	private String code;
	private String message;

	public ResponseBean(String code) {
		this.code = code;
	}

	public ResponseBean(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ResponseBean [code=" + code + ", message=" + message + "]";
	}

}
