package com.serkan.service.reporting.controller.api;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.serkan.service.reporting.model.CustomerInfoResponse;
import com.serkan.service.reporting.model.MerchantResponse;
import com.serkan.service.reporting.model.TransactionQueryRequest;
import com.serkan.service.reporting.model.TransactionQueryResponse;
import com.serkan.service.reporting.model.TransactionReportRequest;
import com.serkan.service.reporting.model.TransactionReportResponse;
import com.serkan.service.reporting.model.TransactionResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "transaction", description = "transaction reporting")
public interface TransactionApi {

	@ApiOperation(value = "get transaction reports", notes = " ", response = TransactionReportResponse.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 500, message = "E_INTERNAL_ERROR"),
            @ApiResponse(code = 200, message = " ", response = TransactionReportResponse.class),
            @ApiResponse(code = 417, message = "E_INVALID_PARAM_VALUE"),
            @ApiResponse(code = 422, message = "E_NOT_VALID_REQUEST") })
	@RequestMapping(value = "/v1/transaction/report", produces = { "application/json" }, method = RequestMethod.POST)
	default ResponseEntity<TransactionReportResponse> getTransactionReport(
			@RequestBody @Valid TransactionReportRequest transactionReportREQ) {
		return new ResponseEntity<TransactionReportResponse>(HttpStatus.OK);
	}

	@ApiOperation(value = "get transactions", notes = " ", response = TransactionQueryResponse.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 500, message = "E_INTERNAL_ERROR"),
            @ApiResponse(code = 200, message = " ", response = TransactionQueryResponse.class),
            @ApiResponse(code = 417, message = "E_INVALID_PARAM_VALUE"),
            @ApiResponse(code = 422, message = "E_NOT_VALID_REQUEST") })
	@RequestMapping(value = "/v1/transaction/list", produces = { "application/json" }, method = RequestMethod.POST)
	default ResponseEntity<TransactionQueryResponse> getTransactions(
			@RequestBody @Valid TransactionQueryRequest transactionQueryRequest) {
		return new ResponseEntity<TransactionQueryResponse>(HttpStatus.OK);
	}

	@ApiOperation(value = "get transaction by id", notes = " ", response = TransactionResponse.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 500, message = "E_INTERNAL_ERROR"),
            @ApiResponse(code = 200, message = " ", response = TransactionResponse.class),
            @ApiResponse(code = 417, message = "E_INVALID_PARAM_VALUE"),
            @ApiResponse(code = 422, message = "E_NOT_VALID_REQUEST") })
	@RequestMapping(value = "/v1/transaction", produces = { "application/json" }, method = RequestMethod.POST)
	default ResponseEntity<TransactionResponse> getTransactionById(
			@RequestParam(value = "transactionId") String transactionId) {
		return new ResponseEntity<TransactionResponse>(HttpStatus.OK);
	}

	@ApiOperation(value = "get customer information of the transaction", notes = " ", response = CustomerInfoResponse.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 500, message = "E_INTERNAL_ERROR"),
            @ApiResponse(code = 200, message = " ", response = CustomerInfoResponse.class),
            @ApiResponse(code = 417, message = "E_INVALID_PARAM_VALUE"),
            @ApiResponse(code = 422, message = "E_NOT_VALID_REQUEST") })
	@RequestMapping(value = "/v1/transaction/customer", produces = { "application/json" }, method = RequestMethod.POST)
	default ResponseEntity<CustomerInfoResponse> getCustomerInfoOfTransaction(
			@RequestParam(value = "transactionId") String transactionId) {
		return new ResponseEntity<CustomerInfoResponse>(HttpStatus.OK);
	}

	@ApiOperation(value = "get merchant of the transaction", notes = " ", response = MerchantResponse.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 500, message = "E_INTERNAL_ERROR"),
            @ApiResponse(code = 200, message = " ", response = MerchantResponse.class),
            @ApiResponse(code = 417, message = "E_INVALID_PARAM_VALUE"),
            @ApiResponse(code = 422, message = "E_NOT_VALID_REQUEST") })
	@RequestMapping(value = "/v1/transaction/merchant", produces = { "application/json" }, method = RequestMethod.POST)
	default ResponseEntity<MerchantResponse> getMerchantOfTransaction(
			@RequestParam(value = "transactionId") String transactionId) {
		return new ResponseEntity<MerchantResponse>(HttpStatus.OK);
	}

}
