package com.serkan.service.reporting.controller.beans;

public enum ResponseCodes {

    E_MISSING_FIELD,

    E_NOT_VALID_REQUEST,

    E_INVALID_PARAM_VALUE,

    E_INTERNAL_ERROR,

    I_OPERATION_SUCCESSFUL

}
