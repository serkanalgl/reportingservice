package com.serkan.service.reporting.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.serkan.service.reporting.controller.beans.ResponseBean;
import com.serkan.service.reporting.controller.beans.ResponseCodes;
import com.serkan.service.reporting.exceptions.InternalErrorException;
import com.serkan.service.reporting.exceptions.NotValidRequestException;

@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseBean exceptionHandler(Exception ex, HttpServletResponse response) {
		if (ex instanceof org.springframework.http.converter.HttpMessageNotReadableException) {
			org.springframework.http.converter.HttpMessageNotReadableException mainException = (HttpMessageNotReadableException) ex;
			Throwable rootCause = mainException.getRootCause();
			// this exception comes from spring if requested parameter is not a
			// valid json
			response.setStatus(422);
			if (rootCause != null) {
				if (rootCause instanceof InvalidFormatException) {
					logger.error(rootCause.getMessage());
					response.setStatus(HttpStatus.FAILED_DEPENDENCY.value());
					return new ResponseBean(ResponseCodes.E_INVALID_PARAM_VALUE.name(), rootCause.getMessage());
				}
				logger.error(rootCause.getMessage());
			} else {
				logger.error(mainException.getMessage());
			}
			return new ResponseBean(ResponseCodes.E_NOT_VALID_REQUEST.name(), mainException.getMessage());
		}
		
		if(ex instanceof org.springframework.web.client.ResourceAccessException ){
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			logger.error(ex.getMessage());
			return new ResponseBean(ResponseCodes.E_INTERNAL_ERROR.name());
		}

		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		logger.error("unexpected error occurred ", ex);
		return new ResponseBean(ResponseCodes.E_INTERNAL_ERROR.name(), ex.getMessage());
	}

	@ExceptionHandler(NotValidRequestException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ResponseBody
	public ResponseBean notValidRequestExceptionHandler(NotValidRequestException ex) {
		return new ResponseBean(ResponseCodes.E_NOT_VALID_REQUEST.name(), ex.getMessage());
	}

	@ExceptionHandler(InternalErrorException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseBean internalServerExceptionHandler(InternalErrorException ex) {
		logger.error("unexpected error occurred", ex);
		return new ResponseBean(ResponseCodes.E_INTERNAL_ERROR.name(), ex.getMessage());
	}
}
