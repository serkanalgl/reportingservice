//package com.serkan.service.reporting.converters;
//
//import java.io.IOException;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.core.ObjectCodec;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.JsonDeserializer;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.serkan.service.reporting.model.ProviderResponseBase;
//import com.serkan.service.reporting.model.ProviderResponseBeanConverter;
//import com.serkan.service.reporting.model.ProviderResponseStatusEnum;
//
//public class ProviderResponseBaseDeserializer extends JsonDeserializer<ProviderResponseBase> {
//
//	private static final Logger logger = LoggerFactory.getLogger(ProviderResponseBaseDeserializer.class);
//
//	
//	@Override
//	public ProviderResponseBase deserialize(JsonParser jsonParser, DeserializationContext arg1)
//			throws IOException, JsonProcessingException {
//
//		ObjectCodec oc = jsonParser.getCodec();
//		JsonNode node = oc.readTree(jsonParser);
//		String textValue = node.toString().replaceAll("\"", "");
//		
//		logger.info("-------------------" + node.toString());
//		
//		
//		ProviderResponseBase providerResponseBeanConverter = new ProviderResponseBase();
//		if(node.has("code")){
//			providerResponseBeanConverter.setCode(node.get("code").toString());
//		}
//		if(node.has("message")){
//			providerResponseBeanConverter.setCode(node.get("message").toString());
//		}
//		if(node.has("reqId")){
//			providerResponseBeanConverter.setCode(node.get("reqId").toString());
//		}
//		if(node.has("status")){
//			providerResponseBeanConverter.setCode(node.get("status").toString());
//		}
//		
//		return providerResponseBeanConverter;
//	}
//
//}
