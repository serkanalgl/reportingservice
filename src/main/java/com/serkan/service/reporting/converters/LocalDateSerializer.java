package com.serkan.service.reporting.converters;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.serkan.service.reporting.model.Constants;

public class LocalDateSerializer extends JsonSerializer<LocalDate> {

	@Override
	public void serialize(LocalDate date, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeString(date.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT)));
	}

}
