package com.serkan.service.reporting.model;

public class MerchantResponse extends ProviderResponseBase{

	private Merchant merchant;

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	@Override
	public String toString() {
		return super.toString() + ", MerchantResponse [merchant=" + merchant + "]";
	}
	
	
}
