package com.serkan.service.reporting.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class TransactionReportRequest {

	@ApiModelProperty(required = true, example = "2017-01-01")
	@JsonFormat(pattern = Constants.DATE_FORMAT)
	private LocalDate fromDate;

	@ApiModelProperty(required = true, example = "2017-01-01")
	@JsonFormat(pattern = Constants.DATE_FORMAT)
	private LocalDate toDate;
	
	private int merchant;
	private int acquirer;

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public int getMerchant() {
		return merchant;
	}

	public void setMerchant(int merchant) {
		this.merchant = merchant;
	}

	public int getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(int acquirer) {
		this.acquirer = acquirer;
	}

	@Override
	public String toString() {
		return "TransactionReportREQ [fromDate=" + fromDate + ", toDate=" + toDate + ", merchant=" + merchant
				+ ", acquirer=" + acquirer + "]";
	}

}
