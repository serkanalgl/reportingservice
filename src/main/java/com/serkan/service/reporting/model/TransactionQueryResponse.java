package com.serkan.service.reporting.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionQueryResponse extends ProviderResponseBase {

	@JsonProperty("per_page")
	private int perPage;

	@JsonProperty("current_page")
	private int currentPage;

	@JsonIgnore
	private String prevPageUrl;

	@JsonIgnore
	private String nextPageUrl;

	private int from;
	private int to;

	private List<TransactionQueryData> data;

	public int getPerPage() {
		return perPage;
	}

	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getPrevPageUrl() {
		return prevPageUrl;
	}

	public void setPrevPageUrl(String prevPageUrl) {
		this.prevPageUrl = prevPageUrl;
	}

	public String getNextPageUrl() {
		return nextPageUrl;
	}

	public void setNextPageUrl(String nextPageUrl) {
		this.nextPageUrl = nextPageUrl;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public List<TransactionQueryData> getData() {
		return data;
	}

	public void setData(List<TransactionQueryData> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TransactionQueryResponse [perPage=" + perPage + ", currentPage=" + currentPage + ", prevPageUrl="
				+ prevPageUrl + ", nextPageUrl=" + nextPageUrl + ", from=" + from + ", to=" + to + ", data=" + data
				+ "]";
	}

}
