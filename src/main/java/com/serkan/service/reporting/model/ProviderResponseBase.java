package com.serkan.service.reporting.model;

public abstract class ProviderResponseBase {

	Integer code;

	String message;

	String reqId;

	ProviderResponseStatusEnum status;
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public ProviderResponseStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ProviderResponseStatusEnum status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ProviderApiBaseResponse [code=" + code + ", message=" + message + ", reqId=" + reqId + ", status="
				+ status + "]";
	}

}
