package com.serkan.service.reporting.model;

import java.util.List;

public class TransactionReportResponse extends ProviderResponseBase{

	private List<TransactionReport> response;

	public List<TransactionReport> getResponse() {
		return response;
	}

	public void setResponse(List<TransactionReport> response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return super.toString() + ", TransactionReport [response=" + response + "]";
	}

}
