package com.serkan.service.reporting.model;

public class FxMerchant {
	
	private long orginalAmount;
	private String orginalCurrency;

	public long getOrginalAmount() {
		return orginalAmount;
	}

	public void setOrginalAmount(long orginalAmount) {
		this.orginalAmount = orginalAmount;
	}

	public String getOrginalCurrency() {
		return orginalCurrency;
	}

	public void setOrginalCurrency(String orginalCurrency) {
		this.orginalCurrency = orginalCurrency;
	}

	@Override
	public String toString() {
		return "FxMerchant [orginalAmount=" + orginalAmount + ", orginalCurrency=" + orginalCurrency + "]";
	}
}
