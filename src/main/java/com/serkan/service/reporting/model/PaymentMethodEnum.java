package com.serkan.service.reporting.model;

public enum PaymentMethodEnum {

	CREDITCARD("​CREDITCARD"), CUP("CUP"), IDEAL("IDEAL"), GIROPAY("GIROPAY"), MISTERCASH("MISTERCASH"), STORED("STORED"), PAYTOCARD("PAYTOCARD");

	private String method;

	PaymentMethodEnum(String method) {
		this.method = method;
	}

	public String getMethod() {
		return method;
	}
}
