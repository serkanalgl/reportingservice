package com.serkan.service.reporting.model;

public class CustomerInfoResponse extends ProviderResponseBase{

	private CustomerInfo customerInfo;

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	@Override
	public String toString() {
		return super.toString() + ", CustomerInfoResponse [customerInfo=" + customerInfo + "]";
	}
	
	
	
}
