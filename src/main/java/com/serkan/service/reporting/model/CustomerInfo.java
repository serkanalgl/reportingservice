package com.serkan.service.reporting.model;

public class CustomerInfo{

	private int id;
	private String email;
	private String billingFirstName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBillingFirstName() {
		return billingFirstName;
	}

	public void setBillingFirstName(String billingFirstName) {
		this.billingFirstName = billingFirstName;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", email=" + email + ", billingFirstName=" + billingFirstName + "]";
	}

}
