package com.serkan.service.reporting.model;

public enum ProviderResponseStatusEnum {

	APPROVED, DECLINED, ERROR, WAITING
}
