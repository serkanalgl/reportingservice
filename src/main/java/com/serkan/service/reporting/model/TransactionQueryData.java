package com.serkan.service.reporting.model;

import java.util.List;

public class TransactionQueryData {

	private CustomerInfo customerInfo;
	private Merchant merchant;
	private Fx fx;
	private List<MerchantTransaction> merchantTransations;
	private List<Acquirer> acquirerTransactions;

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public Fx getFx() {
		return fx;
	}

	public void setFx(Fx fx) {
		this.fx = fx;
	}

	public List<MerchantTransaction> getMerchantTransations() {
		return merchantTransations;
	}

	public void setMerchantTransations(List<MerchantTransaction> merchantTransations) {
		this.merchantTransations = merchantTransations;
	}

	public List<Acquirer> getAcquirerTransactions() {
		return acquirerTransactions;
	}

	public void setAcquirerTransactions(List<Acquirer> acquirerTransactions) {
		this.acquirerTransactions = acquirerTransactions;
	}

	@Override
	public String toString() {
		return "TransactionQueryData [customerInfo=" + customerInfo + ", merchant=" + merchant + ", fx=" + fx
				+ ", merchantTransations=" + merchantTransations + ", acquirerTransactions=" + acquirerTransactions
				+ "]";
	}

}
