package com.serkan.service.reporting.model;

public class MerchantTransaction {

	private String referenceNo;
	private String status;

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "MerchantTransaction [referenceNo=" + referenceNo + ", status=" + status + "]";
	}

}
