package com.serkan.service.reporting.model;

public enum OperationEnum {
	DIRECT("DIRECT"), REFUND("REFUND"), THREED("3D"), THREEDAUTH("3DAUTH"), STORED("STORED");

	private String operation;

	OperationEnum(String operation) {
		this.operation = operation;
	}

	public String getOperation() {
		return operation;
	}
}
