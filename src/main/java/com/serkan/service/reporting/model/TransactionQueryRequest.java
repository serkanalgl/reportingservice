package com.serkan.service.reporting.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class TransactionQueryRequest {

	@ApiModelProperty(required = true, example = "2017-01-01")
	@JsonFormat(pattern = Constants.DATE_FORMAT)
	private LocalDate fromDate;

	@ApiModelProperty(required = true, example = "2017-01-01")
	@JsonFormat(pattern = Constants.DATE_FORMAT)
	private LocalDate toDate;

	private ProviderResponseStatusEnum status;
	private PaymentMethodEnum paymentMethod;
	private OperationEnum operation;

	private int merchantId;
	private int acquirerId;

	private String errorCode;

	private String filterField;
	private String filterValue;

	private int page;

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public ProviderResponseStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ProviderResponseStatusEnum status) {
		this.status = status;
	}

	public PaymentMethodEnum getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethodEnum paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public OperationEnum getOperation() {
		return operation;
	}

	public void setOperation(OperationEnum operation) {
		this.operation = operation;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public int getAcquirerId() {
		return acquirerId;
	}

	public void setAcquirerId(int acquirerId) {
		this.acquirerId = acquirerId;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getFilterField() {
		return filterField;
	}

	public void setFilterField(String filterField) {
		this.filterField = filterField;
	}

	public String getFilterValue() {
		return filterValue;
	}

	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public String toString() {
		return "TransactionQueryRequest [fromDate=" + fromDate + ", toDate=" + toDate + ", status=" + status
				+ ", paymentMethod=" + paymentMethod + ", operation=" + operation + ", merchantId=" + merchantId
				+ ", acquirerId=" + acquirerId + ", errorCode=" + errorCode + ", filterField=" + filterField
				+ ", filterValue=" + filterValue + ", page=" + page + "]";
	}

}
