package com.serkan.service.reporting.model;

public class ProviderAuthResponse extends ProviderResponseBase{

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
