package com.serkan.service.reporting.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfig {

	@Value("${provider.url}")
	private String providerUrl;

	@Value("${provider.authentication.email}")
	private String providerAuthenticationEmail;

	@Value("${provider.authentication.password}")
	private String providerAuthenticationPassword;

	public String getProviderUrl() {
		return providerUrl;
	}

	public void setProviderUrl(String providerUrl) {
		this.providerUrl = providerUrl;
	}

	public String getProviderAuthenticationEmail() {
		return providerAuthenticationEmail;
	}

	public void setProviderAuthenticationEmail(String providerAuthenticationEmail) {
		this.providerAuthenticationEmail = providerAuthenticationEmail;
	}

	public String getProviderAuthenticationPassword() {
		return providerAuthenticationPassword;
	}

	public void setProviderAuthenticationPassword(String providerAuthenticationPassword) {
		this.providerAuthenticationPassword = providerAuthenticationPassword;
	}

	@Override
	public String toString() {
		return "ApplicationConfig [providerUrl=" + providerUrl + ", providerAuthenticationEmail="
				+ providerAuthenticationEmail + ", providerAuthenticationPassword=" + providerAuthenticationPassword
				+ "]";
	}

}
