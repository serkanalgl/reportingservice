package com.serkan.service.reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.serkan.service.reporting.auth.TokenSupplier;

@Component
public class ContextRefreshListener implements ApplicationListener<ContextRefreshedEvent> {

	private final TokenSupplier tokenSupplier;

	@Autowired
	public ContextRefreshListener(TokenSupplier tokenSupplier) {
		this.tokenSupplier = tokenSupplier;
	}


	@Override
	public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
		tokenSupplier.loadAccessToken();
	}

}
