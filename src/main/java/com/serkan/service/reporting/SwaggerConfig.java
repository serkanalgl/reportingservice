package com.serkan.service.reporting;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {                                    
   
	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
                .apis(RequestHandlerSelectors.basePackage("com.serkan.service.reporting.controller"))
		        .build()
		        .directModelSubstitute( LocalDate.class, String.class )
		        .produces(new HashSet<String>(Arrays.asList(MediaType.APPLICATION_JSON_UTF8_VALUE))) 
		        .consumes(new HashSet<String>(Arrays.asList(MediaType.APPLICATION_JSON_UTF8_VALUE)))
		        .apiInfo(apiInfo())
		        .useDefaultResponseMessages(false);
	}

	private ApiInfo apiInfo() {
		final ApiInfoBuilder builder = new ApiInfoBuilder();
	    builder
	    	.title("Payment Reporting Service Docs")
	    	.version("1.0")
	    	.description("The API provides a reporting endpoint for payment");

	    return builder.build();
	}
	
}
