package com.serkan.service.reporting.exceptions;

@SuppressWarnings("serial")
public class NotValidRequestException extends RuntimeException{

    private final String message;

    public NotValidRequestException( String message ){
        super( message );
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

}
