package com.serkan.service.reporting.exceptions;

@SuppressWarnings("serial")
public class InternalErrorException extends RuntimeException{

    private final String message;

    public InternalErrorException( String message ){
        super( message );
        this.message = message;
    }

    public InternalErrorException( String message, Exception exception ){
        super( message, exception );
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

}
