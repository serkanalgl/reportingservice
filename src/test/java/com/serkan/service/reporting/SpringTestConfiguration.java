package com.serkan.service.reporting;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.serkan.service.reporting.auth.ScheduledTask;
import com.serkan.service.reporting.auth.TokenStore;
import com.serkan.service.reporting.converters.LocalDateDeserializer;
import com.serkan.service.reporting.converters.LocalDateSerializer;
import com.serkan.service.reporting.interceptor.AuthenticationClientHttpRequestInterceptor;
import com.serkan.service.reporting.interceptor.LoggingClientHttpRequestInterceptor;

@SpringBootConfiguration
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan(basePackages = "com.serkan.service.reporting", excludeFilters = {})
public class SpringTestConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(SpringTestConfiguration.class);

	@Bean
	TokenStore tokenStore() {
		return new TokenStore();
	}

	@Bean
	public TestRestTemplate testRestTemplate() {

		TestRestTemplate restTemplate = new TestRestTemplate();
		restTemplate.getRestTemplate()
				.setMessageConverters(Collections.singletonList(mappingJackson2HttpMessageConverter()));

		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		interceptors.add(new AuthenticationClientHttpRequestInterceptor(tokenStore()));
		interceptors.add(new LoggingClientHttpRequestInterceptor());

		restTemplate.getRestTemplate().setInterceptors(interceptors);
		return restTemplate;
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion( JsonInclude.Include.NON_NULL );
        mapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );

		SimpleModule module = new SimpleModule();

		module.addSerializer(LocalDate.class, new LocalDateSerializer());
		module.addDeserializer(LocalDate.class, new LocalDateDeserializer());

		mapper.registerModule(module);
		converter.setObjectMapper(mapper);

		return converter;
	}

}
