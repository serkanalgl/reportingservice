package com.serkan.service.reporting;

import static java.nio.charset.Charset.forName;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.serkan.service.reporting.beans.ApplicationConfig;

import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = SpringTestConfiguration.class)
public abstract class BasicTestSuite {

	@Value("${local.server.port}")
	private int port;

	public String getBaseUrl() {
		return "http://localhost:" + port + "/api";
	}

	@Autowired
	protected RestTemplate testRestTemplate;

	@Autowired
	protected ApplicationConfig applicationConfig;

	protected static EnhancedRandom enhancedRandom;

	@BeforeClass
	public static void init() {
		enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder().overrideDefaultInitialization(true)
				.minCollectionSize(5).maxCollectionSize(10).minStringLength(10).maxStringLength(20)
				.maxRandomizationDepth(4).charset(forName("UTF-8")).build();
	}

}