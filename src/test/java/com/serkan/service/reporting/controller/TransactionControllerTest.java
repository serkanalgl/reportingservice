package com.serkan.service.reporting.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.serkan.service.reporting.BasicTestSuite;
import com.serkan.service.reporting.auth.TokenStore;
import com.serkan.service.reporting.handler.impl.util.RestUtil;
import com.serkan.service.reporting.model.ProviderResponseStatusEnum;
import com.serkan.service.reporting.model.TransactionReportRequest;
import com.serkan.service.reporting.model.TransactionReportResponse;
import com.serkan.service.reporting.model.TransactionResponse;

public class TransactionControllerTest extends BasicTestSuite {

	private static final Logger logger = LoggerFactory.getLogger(TransactionControllerTest.class);

	@Test
	public void getTransactionReport() {

		TransactionReportRequest request = new TransactionReportRequest();
		request.setAcquirer(1);
		request.setMerchant(1);
		request.setFromDate(LocalDate.of(2015, Month.JANUARY, 1));
		request.setToDate(LocalDate.of(2015, Month.APRIL, 1));

		HttpEntity<TransactionReportRequest> entity = new HttpEntity<TransactionReportRequest>(request);

		// execute
		ResponseEntity<TransactionReportResponse> responseEntity = testRestTemplate.exchange(
				getBaseUrl() + "/v1/transaction/report", HttpMethod.POST, entity, TransactionReportResponse.class);

		// assert
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(responseEntity.getBody().getStatus()).isEqualTo(ProviderResponseStatusEnum.APPROVED);
	}

}
